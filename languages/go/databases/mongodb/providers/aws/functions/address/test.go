// Created by Jonee Ryan Ty
// Copyright ACloudApp

package main

import (
	"context"
	"log"
	"time"

	// acaGoConfiguration "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/configuration"
	acaGoMongoDBModels "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/models"
	// acaGoMongoDBAWSUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/providers/aws/utilities"
	acaGoMongoDBUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/utilities"
	// acaGoUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/utilities"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
	// "go.mongodb.org/mongo-driver/mongo/readpref"
)

func main() {
	var mongoClient *mongo.Client

	mapStore := make(map[string]interface{})
	mapStore["last_milestone_time"] = time.Now()
	mapStore["stage"] = "dev"
	mapStore["mongoClient"] = mongoClient
	acaGoMongoDBUtilities.DoDBConnect(mapStore)
	mongoClient = mapStore["mongoClient"].(*mongo.Client) // save it back

	addressCol := mapStore["addressCol"].(*mongo.Collection)

	ctx := context.Background()

	search := "1600"

	regexFilter := "" + search + ""

	//    filter := bson.M{"line1": bson.M{"$regex": primitive.Regex{Pattern:regexFilter, Options:"i"}}}
	filter := bson.M{"$or": []bson.M{
		bson.M{"line1": bson.M{"$regex": primitive.Regex{Pattern: regexFilter, Options: "i"}}},
		bson.M{"line2": bson.M{"$regex": primitive.Regex{Pattern: regexFilter, Options: "i"}}},
		bson.M{"city": bson.M{"$regex": primitive.Regex{Pattern: regexFilter, Options: "i"}}},
		bson.M{"state": bson.M{"$regex": primitive.Regex{Pattern: regexFilter, Options: "i"}}},
		bson.M{"zip": bson.M{"$regex": primitive.Regex{Pattern: regexFilter, Options: "i"}}},
	}}

	retAddresses := make([]map[string]interface{}, 0)
	cursor, err := addressCol.Find(ctx, filter)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		log.Println(1)
		var tmpAddress acaGoMongoDBModels.Address
		if err = cursor.Decode(&tmpAddress); err != nil {
			log.Fatal(err)
		}
		retAddresses = append(retAddresses, tmpAddress.ExportArrayPublic())
	}

	defer mongoClient.Disconnect(context.TODO())

	log.Println(retAddresses)

	// main
}
