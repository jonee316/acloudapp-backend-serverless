// Created by Jonee Ryan Ty
// Copyright ACloudApp

// address_create

package main

import (
	"context"
	"log"
	"time"

	"net/http"

	// acaGoConfiguration "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/configuration"
	acaGoMongoDBModels "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/models"
	acaGoMongoDBAWSUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/providers/aws/utilities"
	acaGoMongoDBUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/utilities"
	acaGoUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/utilities"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
	// "go.mongodb.org/mongo-driver/mongo/readpref"
)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
// type Response events.APIGatewayProxyResponse

var t0 = time.Now()
var mongoClient *mongo.Client

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	t1 := time.Now()
	log.Println("0-1 lambda life", (t1.UnixNano()-t0.UnixNano())/int64(time.Millisecond))

	mapStore := make(map[string]interface{})
	mapStore["last_milestone_time"] = t1

	var err error
	hasError := false

	hasError, response := acaGoMongoDBAWSUtilities.DoInit(mapStore, request, true)
	if hasError {
		return acaGoMongoDBAWSUtilities.DoResponse(response, ""), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "do init")

	pp := mapStore["post_parameters"].(map[string]interface{})

	// parameters
	line1, _ := pp["line1"].(string)
	line2, _ := pp["line2"].(string)
	city, _ := pp["city"].(string)
	state, _ := pp["state"].(string)
	zip, _ := pp["zip"].(string)
	// log.Println("line1, line2, city, state, zip:", line1, line2, city, state, zip)

	// validate parameters
	formErrors := make(map[string]interface{})
	hasError = false

	// validate line1
	line1Errors := make([]map[string]interface{}, 0)
	if line1 == "" {
		line1Errors = append(line1Errors, map[string]interface{}{"message_key": "ERROR_LINE1_REQUIRED", "message": "line1 is required"})
		hasError = true
	}

	if len(line1Errors) > 0 {
		formErrors["line1"] = line1Errors
	}

	// line2 is not required

	// validate city
	cityErrors := make([]map[string]interface{}, 0)
	if city == "" {
		cityErrors = append(cityErrors, map[string]interface{}{"message_key": "ERROR_CITY_REQUIRED", "message": "city is required"})
		hasError = true
	}

	if len(cityErrors) > 0 {
		formErrors["city"] = cityErrors
	}

	// validate state
	stateErrors := make([]map[string]interface{}, 0)
	if state == "" {
		stateErrors = append(stateErrors, map[string]interface{}{"message_key": "ERROR_STATE_REQUIRED", "message": "state is required"})
		hasError = true
	}

	if len(stateErrors) > 0 {
		formErrors["state"] = stateErrors
	}

	// validate zip
	zipErrors := make([]map[string]interface{}, 0)
	if zip == "" {
		zipErrors = append(zipErrors, map[string]interface{}{"message_key": "ERROR_ZIP_REQUIRED", "message": "zip is required"})
		hasError = true
	}

	if len(zipErrors) > 0 {
		formErrors["zip"] = zipErrors
	}

	if hasError {
		return acaGoMongoDBAWSUtilities.DoResponse(
			map[string]interface{}{
				"statusCode": http.StatusBadRequest,
				"body":       acaGoUtilities.GetFormErrorsJsonStringBodyReturn(false, "ERROR_VALIDATION", formErrors),
			},
			"",
		), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "parameters")

	// should be done with simple validation still need to check db duplicates

	acaGoMongoDBUtilities.DoDBConnect(mapStore)
	acaGoUtilities.PrintMilestone(mapStore, "db")

	mongoClient = mapStore["mongoClient"].(*mongo.Client) // retrieve reference

	addressCol := mapStore["addressCol"].(*mongo.Collection)

	ctx := context.Background()

	// find same address if any existing
	filter := bson.M{"$and": []bson.M{bson.M{"line1": line1}, bson.M{"line2": line2}, bson.M{"zip": zip}}}
	var tmpAddressObj acaGoMongoDBModels.Address
	err = addressCol.FindOne(ctx, filter).Decode(&tmpAddressObj)
	if err == nil { // object found so we return an error
		line1Errors = append(line1Errors, map[string]interface{}{"message_key": "ERROR_ADDRESS_EXISTS", "message": "address is in database"})
		formErrors["line1"] = line1Errors
		hasError = true

		return acaGoMongoDBAWSUtilities.DoResponse(
			map[string]interface{}{
				"statusCode": http.StatusBadRequest,
				"body":       acaGoUtilities.GetFormErrorsJsonStringBodyReturn(false, "ERROR_VALIDATION", formErrors),
			},
			"",
		), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "address db duplicates check")

	// all validations should now be done finally do save
	var addressObj acaGoMongoDBModels.Address
	addressObj.Line1 = line1
	addressObj.Line2 = line2
	addressObj.City = city
	addressObj.State = state
	addressObj.Zip = zip

	addressObj.CreatedAt = time.Now()

	acaGoUtilities.PrintMilestone(mapStore, "preparation before save")

	_, err = addressObj.Save(mapStore)
	if err != nil {
		log.Println("ERROR", err)

		log.Println("likely database down")

		return acaGoMongoDBAWSUtilities.DoResponse(
			map[string]interface{}{
				"statusCode": http.StatusInternalServerError,
				"body":       acaGoUtilities.GetSimpleJsonStringBodyReturn(false, "ERROR_INTERNAL", nil),
			},
			"",
		), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "db saving / create address")

	log.Println("function exec", (time.Now().UnixNano()-t1.UnixNano())/int64(time.Millisecond))

	// return success
	return acaGoMongoDBAWSUtilities.DoResponse(
		map[string]interface{}{
			"statusCode": http.StatusOK,
			"body":       acaGoUtilities.GetJsonStringBodyReturn(true, addressObj.ExportArrayPublic()),
		},
		"",
	), nil

}

func main() {
	lambda.Start(Handler)
}
