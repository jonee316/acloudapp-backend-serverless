// Created by Jonee Ryan Ty
// Copyright ACloudApp

// address_read

package main

import (
	"context"
	"log"
	"time"

	"net/http"

	// acaGoConfiguration "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/configuration"
	acaGoMongoDBModels "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/models"
	acaGoMongoDBAWSUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/providers/aws/utilities"
	acaGoMongoDBUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/utilities"
	acaGoUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/utilities"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
	// "go.mongodb.org/mongo-driver/mongo/readpref"
)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
// type Response events.APIGatewayProxyResponse

var t0 = time.Now()
var mongoClient *mongo.Client

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	t1 := time.Now()
	log.Println("0-1 lambda life", (t1.UnixNano()-t0.UnixNano())/int64(time.Millisecond))

	mapStore := make(map[string]interface{})
	mapStore["last_milestone_time"] = t1

	var err error
	hasError := false

	hasError, response := acaGoMongoDBAWSUtilities.DoInit(mapStore, request, false)
	if hasError {
		return acaGoMongoDBAWSUtilities.DoResponse(response, ""), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "do init")

	// path parameter
	pathParameters := request.PathParameters
	addressIdStr, _ := pathParameters["address_id"]
	// log.Println("addressIdStr:", addressIdStr)

	if addressIdStr == "" {
		return acaGoMongoDBAWSUtilities.DoResponse(
			map[string]interface{}{
				"statusCode": http.StatusBadRequest,
				"body":       acaGoUtilities.GetSimpleJsonStringBodyReturn(false, "ERROR_BAD_REQUEST", nil),
			},
			"text/html",
		), nil
	}

	addressId, err := primitive.ObjectIDFromHex(addressIdStr)
	// check bson ids
	if err != nil {
		return acaGoMongoDBAWSUtilities.DoResponse(
			map[string]interface{}{
				"statusCode": http.StatusBadRequest,
				"body":       acaGoUtilities.GetSimpleJsonStringBodyReturn(false, "ERROR_BAD_REQUEST", nil),
			},
			"text/html",
		), nil
	}

	acaGoMongoDBUtilities.DoDBConnect(mapStore)
	acaGoUtilities.PrintMilestone(mapStore, "db")

	mongoClient = mapStore["mongoClient"].(*mongo.Client) // retrieve reference

	addressCol := mapStore["addressCol"].(*mongo.Collection)

	ctx := context.Background()

	// find address record with given addressIdStr if existing
	f := bson.M{"_id": addressId}
	var addressObj acaGoMongoDBModels.Address
	err = addressCol.FindOne(ctx, f).Decode(&addressObj)
	if err != nil { // address not found
		return acaGoMongoDBAWSUtilities.DoResponse(
			map[string]interface{}{
				"statusCode": http.StatusNotFound,
				"body":       acaGoUtilities.GetSimpleJsonStringBodyReturn(false, "ERROR_USER_NOT_FOUND", nil),
			},
			"",
		), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "db lookup record")

	log.Println("function exec", (time.Now().UnixNano()-t1.UnixNano())/int64(time.Millisecond))

	// return success
	return acaGoMongoDBAWSUtilities.DoResponse(
		map[string]interface{}{
			"statusCode": http.StatusOK,
			"body":       acaGoUtilities.GetJsonStringBodyReturn(true, addressObj.ExportArrayPublic()),
		},
		"",
	), nil

}

func main() {
	lambda.Start(Handler)
}
