package main

import (
	"context"
	"log"
	"strconv"
	"time"

	"net/http"

	// acaGoConfiguration "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/configuration"
	acaGoMongoDBModels "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/models"
	acaGoMongoDBAWSUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/providers/aws/utilities"
	acaGoMongoDBUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/utilities"
	acaGoUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/utilities"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	// "go.mongodb.org/mongo-driver/mongo/readpref"
)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
// type Response events.APIGatewayProxyResponse

var t0 time.Time
var mongoClient *mongo.Client

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	if t0.IsZero() {
		t0 = time.Now()
		log.Println("new t0")
	}

	t1 := time.Now()
	log.Println("0-1 lambda life", (t1.UnixNano()-t0.UnixNano())/int64(time.Millisecond))

	mapStore := make(map[string]interface{})
	mapStore["last_milestone_time"] = t1

	var err error
	hasError := false

	// we need the post parameters
	hasError, response := acaGoMongoDBAWSUtilities.DoInit(mapStore, request, false)
	if hasError {
		return acaGoMongoDBAWSUtilities.DoResponse(response, ""), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "do init")

	// get parameters
	queryStringParameters := request.QueryStringParameters
	limitStr, hasLimit := queryStringParameters["limit"]
	limit, _ := strconv.Atoi(limitStr)
	if !hasLimit || limit < 0 {
		limit = 0
	}
	pageStr, hasPage := queryStringParameters["page"]
	page, _ := strconv.Atoi(pageStr)
	if !hasPage || page < 1 {
		page = 1
	}
	log.Println("limit, page:", limit, page)

	// step 1. retrieve post parameters and validate
	pp := mapStore["post_parameters"].(map[string]interface{})
	search, _ := pp["search"].(string)
	log.Println("search:", search)

	// validate parameter(s)
	formErrors := make(map[string]interface{})

	// validate parameter search
	searchErrors := make([]map[string]interface{}, 0)
	if search == "" {
		searchErrors = append(searchErrors, map[string]interface{}{"message_key": "ERROR_SEARCH_REQUIRED", "message": "search is required"})
		hasError = true

	} else if len(search) < 2 {
		searchErrors = append(searchErrors, map[string]interface{}{"message_key": "ERROR_SEARCH_MIN_LENGTH", "message": "search should be at least 2 characters"})
		hasError = true
	}

	if len(searchErrors) > 0 {
		formErrors["search"] = searchErrors
	}

	if hasError {
		return acaGoMongoDBAWSUtilities.DoResponse(
			map[string]interface{}{
				"statusCode": http.StatusBadRequest,
				"body":       acaGoUtilities.GetFormErrorsJsonStringBodyReturn(false, "ERROR_VALIDATION", formErrors),
			},
			"",
		), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "parameters")

	// validation done

	acaGoMongoDBUtilities.DoDBConnect(mapStore)
	acaGoUtilities.PrintMilestone(mapStore, "db")

	mongoClient = mapStore["mongoClient"].(*mongo.Client) // retrieve reference

	addressCol := mapStore["addressCol"].(*mongo.Collection)

	ctx := context.Background()

	// step 2. actual mongodb case insensitive constains search
	regexFilter := search
	filter := bson.M{"$or": []bson.M{
		bson.M{"line1": bson.M{"$regex": primitive.Regex{Pattern: regexFilter, Options: "i"}}},
		bson.M{"line2": bson.M{"$regex": primitive.Regex{Pattern: regexFilter, Options: "i"}}},
		bson.M{"city": bson.M{"$regex": primitive.Regex{Pattern: regexFilter, Options: "i"}}},
		bson.M{"state": bson.M{"$regex": primitive.Regex{Pattern: regexFilter, Options: "i"}}},
		bson.M{"zip": bson.M{"$regex": primitive.Regex{Pattern: regexFilter, Options: "i"}}},
	}}

	retAddresses := make([]map[string]interface{}, 0)

	var cursor *mongo.Cursor
	var skip = limit * (page - 1)

	if limit > 0 {
		var skip64 = int64(skip)
		var limit64 = int64(limit)

		opts := options.FindOptions{
			Skip:  &skip64,
			Limit: &limit64,
		}

		cursor, err = addressCol.Find(ctx, filter, &opts)

	} else {
		cursor, err = addressCol.Find(ctx, filter)
	}
	if err != nil {
		log.Fatal(err)
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var tmpAddress acaGoMongoDBModels.Address
		if err = cursor.Decode(&tmpAddress); err != nil {
			log.Fatal(err)
		}
		retAddresses = append(retAddresses, tmpAddress.ExportArrayPublic())
	}

	acaGoUtilities.PrintMilestone(mapStore, "search")

	log.Println("function exec", (time.Now().UnixNano()-t1.UnixNano())/int64(time.Millisecond))

	ret := make(map[string]interface{})
	ret["data"] = retAddresses

	// return success
	AWSresponse := events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       acaGoUtilities.GetJsonStringBodyReturn(true, ret),
		Headers:    map[string]string{"Content-Type": "application/json", "Access-Control-Allow-Origin": "*"},
	}
	return AWSresponse, nil

}

func main() {
	lambda.Start(Handler)
}
