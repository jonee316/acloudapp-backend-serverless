// Created by Jonee Ryan Ty
// Copyright ACloudApp

// address_list

package main

import (
	"context"
	"log"
	"strconv"
	"time"

	"net/http"

	// acaGoConfiguration "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/configuration"
	acaGoMongoDBModels "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/models"
	acaGoMongoDBAWSUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/providers/aws/utilities"
	acaGoMongoDBUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/utilities"
	acaGoUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/utilities"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"go.mongodb.org/mongo-driver/bson"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	// "go.mongodb.org/mongo-driver/mongo/readpref"
)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
// type Response events.APIGatewayProxyResponse

var t0 = time.Now()
var mongoClient *mongo.Client

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	t1 := time.Now()
	log.Println("0-1 lambda life", (t1.UnixNano()-t0.UnixNano())/int64(time.Millisecond))

	mapStore := make(map[string]interface{})
	mapStore["last_milestone_time"] = t1

	var err error
	hasError := false

	hasError, response := acaGoMongoDBAWSUtilities.DoInit(mapStore, request, false)
	if hasError {
		return acaGoMongoDBAWSUtilities.DoResponse(response, ""), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "do init")

	// get parameters
	queryStringParameters := request.QueryStringParameters
	limitStr, hasLimit := queryStringParameters["limit"]
	limit, _ := strconv.Atoi(limitStr)
	if !hasLimit || limit < 0 {
		limit = 0
	}
	pageStr, hasPage := queryStringParameters["page"]
	page, _ := strconv.Atoi(pageStr)
	if !hasPage || page < 1 {
		page = 1
	}
	log.Println("limit, page:", limit, page)

	acaGoMongoDBUtilities.DoDBConnect(mapStore)
	acaGoUtilities.PrintMilestone(mapStore, "db")

	mongoClient = mapStore["mongoClient"].(*mongo.Client) // retrieve reference

	addressCol := mapStore["addressCol"].(*mongo.Collection)

	ctx := context.Background()

	// find addresses
	retAddresses := make([]map[string]interface{}, 0)

	var cursor *mongo.Cursor
	var skip = limit * (page - 1)

	if limit > 0 {
		var skip64 = int64(skip)
		var limit64 = int64(limit)

		opts := options.FindOptions{
			Skip:  &skip64,
			Limit: &limit64,
		}

		cursor, err = addressCol.Find(ctx, bson.M{}, &opts)

	} else {
		cursor, err = addressCol.Find(ctx, bson.M{})
	}
	if err != nil {
		log.Fatal(err)
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var tmpAddress acaGoMongoDBModels.Address
		if err = cursor.Decode(&tmpAddress); err != nil {
			log.Fatal(err)
		}
		retAddresses = append(retAddresses, tmpAddress.ExportArrayPublic())
	}

	acaGoUtilities.PrintMilestone(mapStore, "decoding / serialization")

	log.Println("function exec", (time.Now().UnixNano()-t1.UnixNano())/int64(time.Millisecond))

	ret := make(map[string]interface{})
	ret["data"] = retAddresses

	// return success
	return acaGoMongoDBAWSUtilities.DoResponse(
		map[string]interface{}{
			"statusCode": http.StatusOK,
			"body":       acaGoUtilities.GetJsonStringBodyReturn(true, ret),
		},
		"",
	), nil

}

func main() {
	lambda.Start(Handler)
}
