# LOB Coding Test
Submitted by Jonee Ryan Ty

# Backend Features
The APIs created are using Serverless Application Framework. Programming language is mainly Go and database used is MongoDB. It is already deployed and active in AWS. It is building on top of previous codes https://gitlab.com/jonee316/acloudapp-backend-serverless

The relevant codes can be found under acloudapp-backend-serverless/languages/go/databases/mongodb/providers/aws/functions/address

# Available end points

1. Create an address 

curl -X POST -d '{"line1":"line1", "line2":"line2", "city":"city", "state":"state", "zip":"zip"}' https://2xnryqgzfa.execute-api.us-east-1.amazonaws.com/api/1/dev/address

{"_id":"5fcaee7079fac4d6a4beb777","city":"city","created_at":1607134832,"line1":"line1","line2":"line2","state":"state","success":1,"zip":"zip"}

2. Read / show an address

curl -X GET https://2xnryqgzfa.execute-api.us-east-1.amazonaws.com/api/1/dev/address/5fcaee7079fac4d6a4beb777

{"_id":"5fcaee7079fac4d6a4beb777","city":"city","created_at":1607134832,"line1":"1 First St.","line2":"line2","state":"state","success":1,"updated_at":1607139265,"zip":"zip"}

3. Update an address

curl -X PUT -d '{"line1":"1 First St.", "line2":"line2", "city":"city", "state":"state", "zip":"zip"}' https://2xnryqgzfa.execute-api.us-east-1.amazonaws.com/api/1/dev/address/5fcaee7079fac4d6a4beb777

{"_id":"5fcaee7079fac4d6a4beb777","city":"city","created_at":1607134832,"line1":"1 First St.","line2":"line2","state":"state","success":1,"updated_at":1607139265,"zip":"zip"}

4. Delete an address

curl -X DELETE https://2xnryqgzfa.execute-api.us-east-1.amazonaws.com/api/1/dev/address/delete/5fcaee7079fac4d6a4beb777

{"address_id":"5fcaee7079fac4d6a4beb777","success":1}

5. List all addresses available in the db

curl -X GET https://2xnryqgzfa.execute-api.us-east-1.amazonaws.com/api/1/dev/address/

{"data":[{"_id":"5fcb0d94be64725c1bdd48c8","city":"San Francisco","created_at":1607142804,"line1":"1600 Holloway Ave","line2":"Suite 20","state":"CA","zip":"94132"},{"_id":"5fcb0d945b7317f856c67a59","city":"San Francisco","created_at":1607142804,"line1":"1600 Holloway Ave","line2":"Suite 10","state":"CA","zip":"94132"},{"_id":"5fcb0d946d2051e9461517ce","city":"San Francisco","created_at":1607142804,"line1":"185 Berry St","line2":"Suite 6100","state":"CA","zip":"94107"}],"success":1}

6. Search from the addresses available in the db

curl -X POST -d '{"search":"1600"}' https://2xnryqgzfa.execute-api.us-east-1.amazonaws.com/api/1/dev/address/search

{"data":[{"_id":"5fcc0f64673a98cf71a5dc69","city":"San Francisco","created_at":1607208804,"line1":"1600 Holloway Ave","state":"CA","zip":"94132"},{"_id":"5fcc0f64350b974cd1c257a4","city":"San Francisco","created_at":1607208804,"line1":"1600 Holloway Ave","line2":"Suite 10","state":"CA","zip":"94132"},{"_id":"5fcc0f646a58267787b4990b","city":"San Francisco","created_at":1607208804,"line1":"1600 Holloway Ave","line2":"Suite 20","state":"CA","zip":"94132"}],"success":1}j

curl -X POST -d '{"search":"MD"}' https://2xnryqgzfa.execute-api.us-east-1.amazonaws.com/api/1/dev/address/search

{"data":[{"_id":"5fcc0f64c42310ad04abc981","city":"Baltimore","created_at":1607208804,"line1":"3400 N. Charles St.","state":"MD","zip":"21218"}],"success":1}

7. Search from the given addresses.json

curl -X POST -d '{"search":"ca"}' https://2xnryqgzfa.execute-api.us-east-1.amazonaws.com/api/1/dev/address/search2

{"data":[{"city":"Cambridge","line1":"Massachusetts Hall","state":"MA","zip":"02138"},{"city":"San Francisco","line1":"1600 Holloway Ave","state":"CA","zip":"94132"},{"city":"San Francisco","line1":"1600 Holloway Ave","line2":"Suite 10","state":"CA","zip":"94132"},{"city":"San Francisco","line1":"1600 Holloway Ave","line2":"Suite 20","state":"CA","zip":"94132"},{"city":"San Francisco","line1":"185 Berry St","line2":"Suite 6100","state":"CA","zip":"94107"}],"success":1}


# Frontend Features
Also available is a quick tester of the search / autocomplete function. It is a simple HTML and jQuery code which should be functional when loaded or opened in your browser. 
