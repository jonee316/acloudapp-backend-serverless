package main

import (
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"encoding/json"
	"io/ioutil"
	"net/http"

	acaGoMongoDBAWSUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/databases/mongodb/providers/aws/utilities"
	acaGoUtilities "gitlab.com/jonee316/acloudapp-backend-serverless/languages/go/utilities"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
// type Response events.APIGatewayProxyResponse

var t0 time.Time
var data []map[string]string

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	if t0.IsZero() {
		t0 = time.Now()
		log.Println("new t0")
	}

	t1 := time.Now()
	log.Println("0-1 lambda life", (t1.UnixNano()-t0.UnixNano())/int64(time.Millisecond))

	mapStore := make(map[string]interface{})
	mapStore["last_milestone_time"] = t1

	// var err error
	hasError := false

	// we need the post parameters
	hasError, response := acaGoMongoDBAWSUtilities.DoInit(mapStore, request, false)
	if hasError {
		return acaGoMongoDBAWSUtilities.DoResponse(response, ""), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "do init")

	// get parameters
	queryStringParameters := request.QueryStringParameters
	limitStr, hasLimit := queryStringParameters["limit"]
	limit, _ := strconv.Atoi(limitStr)
	if !hasLimit || limit < 0 {
		limit = 0
	}
	pageStr, hasPage := queryStringParameters["page"]
	page, _ := strconv.Atoi(pageStr)
	if !hasPage || page < 1 {
		page = 1
	}
	log.Println("limit, page:", limit, page)

	// step 1. retrieve post parameters and validate
	pp := mapStore["post_parameters"].(map[string]interface{})
	search, _ := pp["search"].(string)
	log.Println("search:", search)

	// validate parameter(s)
	formErrors := make(map[string]interface{})

	// validate parameter search
	searchErrors := make([]map[string]interface{}, 0)
	if search == "" {
		searchErrors = append(searchErrors, map[string]interface{}{"message_key": "ERROR_SEARCH_REQUIRED", "message": "search is required"})
		hasError = true

	} else if len(search) < 2 {
		searchErrors = append(searchErrors, map[string]interface{}{"message_key": "ERROR_SEARCH_MIN_LENGTH", "message": "search should be at least 2 characters"})
		hasError = true
	}

	if len(searchErrors) > 0 {
		formErrors["search"] = searchErrors
	}

	if hasError {
		return acaGoMongoDBAWSUtilities.DoResponse(
			map[string]interface{}{
				"statusCode": http.StatusBadRequest,
				"body":       acaGoUtilities.GetFormErrorsJsonStringBodyReturn(false, "ERROR_VALIDATION", formErrors),
			},
			"",
		), nil
	}

	acaGoUtilities.PrintMilestone(mapStore, "parameters")

	// validation done

	// step 2. load our "database" if needed

	if data == nil || len(data) == 0 {
		// open json file thanks https://tutorialedge.net/golang/parsing-json-with-golang/
		jsonFile, err := os.Open("addresses.json")
		if err != nil {
			log.Println(err)

			return acaGoMongoDBAWSUtilities.DoResponse(
				map[string]interface{}{
					"statusCode": http.StatusInternalServerError,
					"body":       acaGoUtilities.GetSimpleJsonStringBodyReturn(false, "ERROR_INTERNAL", nil),
				},
				"",
			), nil
		}
		defer jsonFile.Close()

		// parse json
		byteValue, _ := ioutil.ReadAll(jsonFile)
		json.Unmarshal(byteValue, &data)

		log.Println("data parsed")

	} else {
		log.Println("reusing our data")
	}

	// step 3. query our database and return results
	results := make([]map[string]string, 0)
	for _, address := range data {
		toAdd := false
		for _, value := range address {
			if strings.Contains(strings.ToLower(value), strings.ToLower(search)) { // string case insensitive search
				toAdd = true
			}
		}
		if toAdd {
			results = append(results, address)
		}
	}

	// pagination
	newResults := make([]map[string]string, 0)
	if limit != 0 {
		skip := limit*(page-1) - 1
		for i := 0; i < len(results); i++ {
			if i > skip && i <= skip+limit {
				newResults = append(newResults, results[i])
			}
		}

	} else {
		newResults = results
	}

	// return success
	AWSresponse := events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       acaGoUtilities.GetJsonStringBodyReturn(true, map[string]interface{}{"data": newResults}),
		Headers:    map[string]string{"Content-Type": "application/json", "Access-Control-Allow-Origin": "*"}, // allows html loaded directly from browser eg file://
	}
	return AWSresponse, nil

}

func main() {
	lambda.Start(Handler)
}
