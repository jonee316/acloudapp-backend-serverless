// Created by Jonee Ryan Ty

/**
 * Address model class
 */

package models

import (
	"context"
	// "errors"
	// "log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
	// "go.mongodb.org/mongo-driver/mongo/readpref"
)

// Address class or struct definition
type Address struct {
	Id primitive.ObjectID `json:"id" bson:"_id,omitempty"`

	Line1 string `json:"line1,omitempty" bson:"line1,omitempty"`
	Line2 string `json:"line2,omitempty" bson:"line2,omitempty"`

	City  string `json:"city,omitempty" bson:"city,omitempty"`
	State string `json:"state,omitempty" bson:"state,omitempty"`
	Zip   string `json:"zip,omitempty" bson:"zip,omitempty"`

	CreatedAt time.Time `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt time.Time `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

// Address class Save function
func (r *Address) Save(mapStore map[string]interface{}) (*Address, error) {
	addressCol := mapStore["addressCol"].(*mongo.Collection)
	myCol := addressCol

	var err error
	ctx := context.Background()

	if r.Id.IsZero() {
		r.Id = primitive.NewObjectID()
		_, err = myCol.InsertOne(ctx, &r)

	} else {
		_, err = myCol.UpdateOne(ctx, bson.M{"_id": r.Id}, bson.M{"$set": r.ExportArrayPrivate()})
	}

	return r, err

	// Save
}

// exportarrayprivate is saveable
func (r *Address) ExportArrayPrivate() map[string]interface{} {
	val := make(map[string]interface{})

	if !r.Id.IsZero() {
		val["_id"] = r.Id
	}

	if r.Line1 != "" {
		val["line1"] = r.Line1
	}

	if r.Line2 != "" {
		val["line2"] = r.Line2
	}

	if r.City != "" {
		val["city"] = r.City
	}

	if r.State != "" {
		val["state"] = r.State
	}

	if r.Zip != "" {
		val["zip"] = r.Zip
	}

	if !r.CreatedAt.IsZero() {
		val["created_at"] = r.CreatedAt
	}

	if !r.UpdatedAt.IsZero() {
		val["updated_at"] = r.UpdatedAt
	}

	return val

	// ExportArrayPrivate
}

// public does not have the passwords and anything sensitive / secure / private, also dates are ints
func (r *Address) ExportArrayPublic() map[string]interface{} {
	val := r.ExportArrayPrivate()

	if !r.Id.IsZero() {
		val["_id"] = r.Id.Hex()
	}

	if !r.CreatedAt.IsZero() {
		val["created_at"] = r.CreatedAt.Unix()
	}

	if !r.UpdatedAt.IsZero() {
		val["updated_at"] = r.UpdatedAt.Unix()
	}

	return val

	// ExportArrayPublic
}
